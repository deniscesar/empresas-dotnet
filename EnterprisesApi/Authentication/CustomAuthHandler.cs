using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace EnterprisesApi.Authentication
{
    public class CustomAuthHandler : AuthenticationHandler<CustomAuthOptions>
    {
        private readonly IUserService _userService;

        public CustomAuthHandler(IOptionsMonitor<CustomAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, IUserService userService) 
            : base(options, logger, encoder, clock)
        {
            _userService = userService;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            // Get header value
            if (!Request.Headers.TryGetValue("access-token", out var accessToken) || !Request.Headers.TryGetValue("client", out var client) || !Request.Headers.TryGetValue("uid", out var uid))
            {
                return Task.FromResult(AuthenticateResult.Fail("Cannot read authorization header."));
            }

            // Validate headers
            User user = _userService.ValidateAccess(accessToken, client, uid);
            if(user == null){
                return Task.FromResult(AuthenticateResult.Fail("Invalid auth key."));
            }
           
            // Create authenticated user
            var identities = new List<ClaimsIdentity> {new ClaimsIdentity("custom auth type")};
            var ticket = new AuthenticationTicket(new ClaimsPrincipal(identities), Options.Scheme);

            return Task.FromResult(AuthenticateResult.Success(ticket));
        }
    }
}
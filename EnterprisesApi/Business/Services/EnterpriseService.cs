using System;
using System.Collections.Generic;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Business.Services
{
    public partial class EnterpriseService : IDisposable, IEnterpriseService
    {
        private readonly IUnitOfWork _unitofwork;

        public EnterpriseService(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        public Enterprise Create(Enterprise entity)
        {
            try
            {
                return _unitofwork.EnterpriseRepository.Create(entity);
            }
            catch (System.Exception ex)
            {  
                throw ex;
            }
        }

        public Enterprise Delete(int id)
        {
            try
            {
                return _unitofwork.EnterpriseRepository.Delete(id);
            }
            catch (System.Exception)
            {
                
                throw;
            }
        }

        public void Dispose()
        {
            _unitofwork.Dispose();
        }

        public List<Enterprise> Get()
        {
            return _unitofwork.EnterpriseRepository.Get();
        }

        public Enterprise Get(int id)
        {
            return _unitofwork.EnterpriseRepository.Get(id);
        }

        public List<Enterprise> GetByTypeName(int type, string name)
        {
            
            try
            {
                return _unitofwork.EnterpriseRepository.GetByTypeName(type, name);
            }
            catch (System.Exception ex)
            {  
                throw ex;
            }            
        }

        public Enterprise Update(Enterprise entity)
        {
            try
            {
                return _unitofwork.EnterpriseRepository.Update(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Business.Services
{
    public partial class EnterpriseTypeService : IDisposable, IEnterpriseTypeService
    {
        private readonly IUnitOfWork _unitofwork;

        public EnterpriseTypeService(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        public EnterpriseType Create(EnterpriseType entity)
        {
            try
            {
                return _unitofwork.EnterpriseTypeRepository.Create(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public EnterpriseType Delete(int id)
        {
            try
            {
                return _unitofwork.EnterpriseTypeRepository.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            _unitofwork.Dispose();
        }

        public List<EnterpriseType> Get()
        {
            try
            {
                return _unitofwork.EnterpriseTypeRepository.Get();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public EnterpriseType Get(int id)
        {
            try
            {
                return _unitofwork.EnterpriseTypeRepository.Get(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public EnterpriseType Update(EnterpriseType entity)
        {
            try
            {
                return _unitofwork.EnterpriseTypeRepository.Update(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Business.Services
{
    public partial class InvestorService : IDisposable, IInvestorService
    {
        private readonly IUnitOfWork _unitofwork;

        public InvestorService(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        public Investor Create(Investor entity)
        {
            try
            {
                return _unitofwork.InvestorRepository.Create(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public Investor Delete(int id)
        {
            try
            {
                return _unitofwork.InvestorRepository.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            _unitofwork.Dispose();
        }

        public List<Investor> Get()
        {
            try
            {
                return _unitofwork.InvestorRepository.Get();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public Investor Get(int id)
        {
            try
            {
                return _unitofwork.InvestorRepository.Get(id);
            }
            catch (System.Exception ex)
            {  
                throw ex;
            }
        }

        public Investor Update(Investor entity)
        {
            try
            {
                return _unitofwork.InvestorRepository.Update(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}

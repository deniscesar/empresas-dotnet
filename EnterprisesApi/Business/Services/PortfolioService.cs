using System;
using System.Collections.Generic;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Business.Services
{
    public partial class PortfolioService : IDisposable, IPortfolioService
    {
        private readonly IUnitOfWork _unitofwork;

        public PortfolioService(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        public Portfolio Create(Portfolio entity)
        {
            try
            {
                return _unitofwork.PortfolioRepository.Create(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public Portfolio Delete(int id)
        {
            try
            {
                return _unitofwork.PortfolioRepository.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            _unitofwork.Dispose();
        }

        public List<Portfolio> Get()
        {
            try
            {
                return _unitofwork.PortfolioRepository.Get();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public Portfolio Get(int id)
        {
            try
            {
                return _unitofwork.PortfolioRepository.Get(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public Portfolio Update(Portfolio entity)
        {
            try
            {
                return _unitofwork.PortfolioRepository.Update(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}

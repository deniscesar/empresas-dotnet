using System;
using System.Collections.Generic;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.ViewModels;

namespace EnterprisesApi.Business.Services
{
    public partial class UserService : IDisposable, IUserService
    {
        private readonly IUnitOfWork _unitofwork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        public ResponseLoginVM Auth(string email, string password)
        {
            try
            {
                ResponseLoginVM data = new ResponseLoginVM();
                User user = _unitofwork.UserRepository.Auth(email, password);
                user.updateAccessData();
                user = _unitofwork.UserRepository.Update(user);
                _unitofwork.Commit();
                Investor investor = _unitofwork.InvestorRepository.Get(user.IdInvestor);
                data.Investor = investor;
                data.User = user;
                return data;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public User ValidateAccess(string accessToken, string client, string uid)
        {
            try
            {
                return _unitofwork.UserRepository.ValidateAccess(accessToken, client, uid);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public User Create(User entity)
        {
            try
            {
                return _unitofwork.UserRepository.Create(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public User Delete(int id)
        {
            try
            {
                return _unitofwork.UserRepository.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            _unitofwork.Dispose();
        }

        public List<User> Get()
        {
            try
            {
                return _unitofwork.UserRepository.Get();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public User Get(int id)
        {
            try
            {
                return _unitofwork.UserRepository.Get(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public User Update(User entity)
        {
            try
            {
                return _unitofwork.UserRepository.Update(entity);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


    }
}

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace EnterprisesApi.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize]
    public class EnterprisesTypeController : Controller
    {
        private readonly IEnterpriseTypeService _enterpriseTypeService;

        public EnterprisesTypeController(IEnterpriseTypeService enterpriseTypeService)
        {
            _enterpriseTypeService = enterpriseTypeService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                List<EnterpriseType> enterpriseTypes = new List<EnterpriseType>();
                enterpriseTypes = _enterpriseTypeService.Get();
                if(enterpriseTypes.Count == 0){
                    return new NoContentResult();
                }else{
                    return new ObjectResult(enterpriseTypes);
                }
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                EnterpriseType enterpriseType = new EnterpriseType();
                enterpriseType = _enterpriseTypeService.Get(id);
                if(enterpriseType == null){
                    return new NoContentResult();
                }else{
                    return new ObjectResult(enterpriseType);
                }
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]EnterpriseType entity)
        {
            try
            {
                EnterpriseType enterpriseType = new EnterpriseType();
                enterpriseType = _enterpriseTypeService.Create(entity);
                return new ObjectResult(enterpriseType);
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                EnterpriseType enterpriseType = new EnterpriseType();
                enterpriseType = _enterpriseTypeService.Delete(id);
                return new ObjectResult(enterpriseType);
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

    }
}

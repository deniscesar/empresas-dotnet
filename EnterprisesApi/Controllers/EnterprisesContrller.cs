using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace EnterprisesApi.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize]
    public class EnterprisesController : Controller
    {
        private readonly IEnterpriseService _enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            _enterpriseService = enterpriseService;
        }

        [HttpGet]
        public object Get(int enterprise_types, string name)
        {
            try
            {
                List<Enterprise> enterpriseList = new List<Enterprise>();
                if(name != null){
                    enterpriseList = _enterpriseService.GetByTypeName(enterprise_types, name);
                }else {
                    enterpriseList = _enterpriseService.Get();
                }

                return new {
                    enterprises = enterpriseList
                };
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public object Get(int id)
        {
            try
            {
                Enterprise enterprise = new Enterprise();
                enterprise = _enterpriseService.Get(id);
                if(enterprise == null){
                    return new NoContentResult();
                }else{
                    return new {
                        enterprise = enterprise,
                        success = true
                    };
                }
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]Enterprise enterprise)
        {
            try
            {
                enterprise = _enterpriseService.Create(enterprise);
                return new ObjectResult(enterprise);
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }        

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                Enterprise enterprise = new Enterprise();
                enterprise = _enterpriseService.Delete(id);
                return new ObjectResult(enterprise);
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }
    }
}

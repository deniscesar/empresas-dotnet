using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace EnterprisesApi.Controllers
{
    [Route("api/v1/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("auth/sign_in")]
        public object Post([FromBody]LoginVM login)
        {
            try
            {
                ResponseLoginVM data = _userService.Auth(login.Email, login.Password);
                if(data != null){
                    Request.HttpContext.Response.Headers.Add("access-token", data.User.AccessToken);
                    Request.HttpContext.Response.Headers.Add("client", data.User.Client);
                    Request.HttpContext.Response.Headers.Add("uid", data.User.Email);

                    return new {
                        data.Investor,
                        success = false,
                    };
                }else{
                    return new{
                        success = false,
                        errors = "Invalid login credentials. Please try again."
                    };
                }
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }

    }
}

using System.Collections.Generic;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Interfaces.Repositories
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {       
        List<Enterprise> GetByTypeName(int type, string name);
    }
}

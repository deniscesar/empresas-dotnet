using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Interfaces.Repositories
{
    public interface IEnterpriseTypeRepository : IRepository<EnterpriseType>
    {

    }
}

using System;
using System.Collections.Generic;

namespace EnterprisesApi.Domain.Interfaces.Repositories
{
    public interface IRepository<T> : IDisposable
    {
        List<T> Get();
        T Get(int id);
        T Create(T entity);
        T Update(T entity);
        T Delete(int id);
    }
}

using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User>
    {       
        User Auth(string email, string password);
        User ValidateAccess(string accessToken, string client, string uid);

    }
}

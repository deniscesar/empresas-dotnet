using System.Collections.Generic;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Interfaces.Services
{
    public interface IEnterpriseService : IService<Enterprise>
    {
        List<Enterprise> GetByTypeName(int type, string name);
    }
}

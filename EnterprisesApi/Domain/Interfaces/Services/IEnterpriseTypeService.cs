using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Interfaces.Services
{
    public interface IEnterpriseTypeService : IService<EnterpriseType>
    {
      
    }
}

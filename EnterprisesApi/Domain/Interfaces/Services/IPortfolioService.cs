using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Interfaces.Services
{
    public interface IPortfolioService : IService<Portfolio>
    {
      
    }
}

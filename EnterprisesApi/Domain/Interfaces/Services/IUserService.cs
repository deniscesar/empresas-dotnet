using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.ViewModels;

namespace EnterprisesApi.Domain.Interfaces.Services
{
    public interface IUserService : IService<User>
    {
        ResponseLoginVM Auth(string email, string password);
        User ValidateAccess(string accessToken, string client, string uid);
    }
}

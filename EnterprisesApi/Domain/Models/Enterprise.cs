using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using Newtonsoft.Json;

namespace EnterprisesApi.Domain.Models
{
    public class Enterprise
    {
        public int Id { get; set; }
        public int? IdPortfolio { get; set; }
        public int IdEnterpriseType { get; set; }
        public string Email_Enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool Own_Enterprise { get; set; }
        public string Enterprise_Name { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Share_Price { get; set; }

        public virtual EnterpriseType EnterpriseType { get; set; }
        public virtual Portfolio Portfolio { get; set; }
    }
    
}
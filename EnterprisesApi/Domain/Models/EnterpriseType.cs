using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using Newtonsoft.Json;

namespace EnterprisesApi.Domain.Models
{
    public class EnterpriseType
    {

        public int Id { get; set; }
        public string Enterprise_Type_Name { get; set; }
        [JsonIgnore]
        public virtual ICollection<Enterprise> Enterprise { get; set; }
    }
    
}
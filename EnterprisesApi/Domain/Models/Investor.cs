using EnterprisesApi.Domain.Models;
using Newtonsoft.Json;

namespace EnterprisesApi.Domain.Models
{
    public class Investor
    {
        public int Id { get; set; }
        public string Investor_Name { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }
        public Portfolio Portfolio { get; set; }
        public decimal Portfolio_Value { get; set; }
        public bool First_Access { get; set; }
        public bool Super_Angel { get; set; }
        
        [JsonIgnore]
        public User User { get; set; }
    }
}
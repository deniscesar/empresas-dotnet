using System.Collections.Generic;
using Newtonsoft.Json;

namespace EnterprisesApi.Domain.Models
{
    public class Portfolio
    {
        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        public int IdInvestor { get; set; }
        public int Enterprises_Number { get; set; }

        public ICollection<Enterprise> Enterprises { get; set; }
        public Investor Investor { get; set; }

        public Portfolio()
        {
            this.Enterprises = new List<Enterprise>();
        }
    }
}
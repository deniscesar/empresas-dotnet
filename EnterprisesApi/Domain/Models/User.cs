using EnterprisesApi.Domain.Helpers;
using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public int IdInvestor { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Client { get; set; }
        public string AccessToken { get; set; }
        public Investor Investor { get; set; }


        public void updateAccessData() {
            AccessToken = Utilites.GenerateToken(22);
            Client = Utilites.GenerateToken(22);
        }
    }
}
using System;
using EnterprisesApi.Domain.Interfaces.Repositories;

namespace EnterprisesApi.Domain.Interfaces.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IEnterpriseRepository EnterpriseRepository { get; }

        IEnterpriseTypeRepository EnterpriseTypeRepository { get; }

        IInvestorRepository InvestorRepository { get; }

        IPortfolioRepository PortfolioRepository { get; }

        IUserRepository UserRepository { get; }
 
        void Commit();
    }
}

using EnterprisesApi.Domain.Models;

namespace EnterprisesApi.Domain.ViewModels
{
    public class ResponseLoginVM
    {
        public Investor Investor { get; set; }
        public User User { get; set; }
    }
}
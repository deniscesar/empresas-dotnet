using System;
using EnterprisesApi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace EnterprisesApi.Infrastructure.DataContexts
{
    public class EnterprisesContext : DbContext
    {       

        public EnterprisesContext() : base()
        {

        }

        public DbSet<Investor> Investor { get; set; }
        public DbSet<Portfolio> Portfolio { get; set; }
        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }
        public DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("Server=denis-mysql.mysql.database.azure.com;port=3306;database=enterprises;uid=deniscesar@denis-mysql;password=Mysql2@19;", mySqlOptions => mySqlOptions.ServerVersion(new Version(5, 0, 12), ServerType.MySql));
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            InvestorMappging(modelBuilder);
            PortfolioMappging(modelBuilder);
            EnterpriseMappging(modelBuilder);
            EnterpriseTypeMappging(modelBuilder);
            UserMappging(modelBuilder);
        }


        #region MAPPINGS

        private void InvestorMappging(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Investor>();

            model.ToTable("Investor");

            //primary key
            model.HasKey(x => x.Id);
            model.Property(x => x.Id).ValueGeneratedOnAdd();

            //properties
            model.Property(x => x.Investor_Name).HasMaxLength(250).IsRequired();
            model.Property(x => x.Email).HasMaxLength(250).IsRequired();
            model.Property(x => x.City).HasMaxLength(250).IsRequired();
            model.Property(x => x.Country).HasMaxLength(250).IsRequired();
            model.Property(x => x.Balance).HasMaxLength(250).IsRequired();
            model.Property(x => x.Photo).HasMaxLength(250);
            model.Property(x => x.Portfolio_Value);
            model.Property(x => x.First_Access).IsRequired();
            model.Property(x => x.Super_Angel).IsRequired();

        }

        private void PortfolioMappging(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Portfolio>();

            model.ToTable("Portfolio");

            //primary key
            model.HasKey(x => x.Id);
            model.Property(x => x.Id).ValueGeneratedOnAdd();

            model.HasOne(x => x.Investor).WithOne(x => x.Portfolio).HasForeignKey<Portfolio>(x => x.IdInvestor).IsRequired().OnDelete(DeleteBehavior.Restrict);

            //properties
            model.Property(x => x.Enterprises_Number).HasMaxLength(250).IsRequired();

        }

        private void EnterpriseMappging(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Enterprise>();

            model.ToTable("Enterprise");

            //primary key
            model.HasKey(x => x.Id);
            model.Property(x => x.Id).ValueGeneratedOnAdd();

            model.HasOne(x => x.Portfolio).WithMany(x => x.Enterprises).HasForeignKey(x => x.IdPortfolio).IsRequired(false);
            model.HasOne(x => x.EnterpriseType).WithMany(x => x.Enterprise).HasForeignKey(x => x.IdEnterpriseType).IsRequired();

            //properties
            model.Property(x => x.Email_Enterprise).HasMaxLength(250);
            model.Property(x => x.Facebook).HasMaxLength(250);
            model.Property(x => x.Twitter).HasMaxLength(250);
            model.Property(x => x.Linkedin).HasMaxLength(250);
            model.Property(x => x.Phone).HasMaxLength(250);
            model.Property(x => x.Own_Enterprise);
            model.Property(x => x.Enterprise_Name).HasMaxLength(250).IsRequired();
            model.Property(x => x.Photo).HasMaxLength(250);
            model.Property(x => x.Description).HasMaxLength(250).IsRequired();
            model.Property(x => x.City).HasMaxLength(250).IsRequired();
            model.Property(x => x.Country).HasMaxLength(250).IsRequired();
            model.Property(x => x.Share_Price).HasMaxLength(250).IsRequired();

        }

        private void EnterpriseTypeMappging(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<EnterpriseType>();

            model.ToTable("EnterpriseType");

            //primary key
            model.HasKey(x => x.Id);
            model.Property(x => x.Id).ValueGeneratedOnAdd();

            //properties
            model.Property(x => x.Enterprise_Type_Name).HasMaxLength(250).IsRequired();

        }

        private void UserMappging(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<User>();

            model.ToTable("User");

            //primary key
            model.HasKey(x => x.Id);
            model.Property(x => x.Id).ValueGeneratedOnAdd();

            model.HasOne(x => x.Investor).WithOne(x => x.User).HasForeignKey<User>(x => x.IdInvestor).IsRequired().OnDelete(DeleteBehavior.Restrict);
            
            //properties
            model.Property(x => x.Email).HasMaxLength(250).IsRequired();
            model.Property(x => x.Password).HasMaxLength(250).IsRequired();
            model.Property(x => x.Client).HasMaxLength(250);
            model.Property(x => x.AccessToken).HasMaxLength(250);

        }

        #endregion;
    }
}

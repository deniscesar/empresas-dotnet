using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.Interfaces.Repositories;
using EnterprisesApi.Infrastructure.DataContexts;
using Microsoft.EntityFrameworkCore;

namespace EnterprisesApi.Infrastructure.Repositories
{
    public partial class EnterpriseRepository : IEnterpriseRepository
    {
        private readonly EnterprisesContext _db;

        public EnterpriseRepository(EnterprisesContext enterprisesContext)
        {
            _db = enterprisesContext;
        }

        public Enterprise Create(Enterprise entity)
        {
            _db.Enterprise.Add(entity);
            _db.SaveChanges();
            return entity;
        }

        public Enterprise Delete(int id)
        {
            var item = _db.Enterprise.FirstOrDefault(a => a.Id == id);
            _db.Enterprise.Remove(item);
            _db.SaveChanges();
            return item;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<Enterprise> Get()
        {
            return _db.Enterprise.Include(x => x.EnterpriseType).OrderBy(x => x.Id).ToList();
        }

        public Enterprise Get(int id)
        {
            return _db.Enterprise.Include(x => x.EnterpriseType).Where(x => x.Id == id).FirstOrDefault();
        }

        public List<Enterprise> GetByTypeName(int type, string name)
        {
            return _db.Enterprise.Include(x => x.EnterpriseType).Where(x => x.EnterpriseType.Id == type && x.Enterprise_Name.ToLower().Contains(name.ToLower())).ToList();
        }

        public Enterprise Update(Enterprise entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();
            return entity;
        }
    }
}

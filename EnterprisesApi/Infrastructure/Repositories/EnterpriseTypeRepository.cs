using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.Interfaces.Repositories;
using EnterprisesApi.Infrastructure.DataContexts;
using Microsoft.EntityFrameworkCore;

namespace EnterprisesApi.Infrastructure.Repositories
{
    public partial class EnterpriseTypeRepository : IEnterpriseTypeRepository
    {
        private readonly EnterprisesContext _db;

        public EnterpriseTypeRepository(EnterprisesContext enterprisesContext)
        {
            _db = enterprisesContext;
        }

        public EnterpriseType Create(EnterpriseType entity)
        {
            _db.EnterpriseType.Add(entity);
            _db.SaveChanges();
            return entity;
        }

        public EnterpriseType Delete(int id)
        {
            var item = _db.EnterpriseType.FirstOrDefault(a => a.Id == id);
            _db.EnterpriseType.Remove(item);
            _db.SaveChanges();
            return item;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<EnterpriseType> Get()
        {
            return _db.EnterpriseType.OrderBy(x => x.Id).ToList();
        }

        public EnterpriseType Get(int id)
        {
            return _db.EnterpriseType.Where(x => x.Id == id).FirstOrDefault();
        }

        public EnterpriseType Update(EnterpriseType entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();
            return entity;
        }
    }
}

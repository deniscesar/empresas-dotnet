using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.Interfaces.Repositories;
using EnterprisesApi.Infrastructure.DataContexts;
using Microsoft.EntityFrameworkCore;

namespace EnterprisesApi.Infrastructure.Repositories
{
    public partial class InvestorRepository : IInvestorRepository
    {
        private readonly EnterprisesContext _db;

        public InvestorRepository(EnterprisesContext enterprisesContext)
        {
            _db = enterprisesContext;
        }

        public Investor Create(Investor entity)
        {
            _db.Investor.Add(entity);
            _db.SaveChanges();
            return entity;
        }

        public Investor Delete(int id)
        {
            var item = _db.Investor.FirstOrDefault(a => a.Id == id);
            _db.Investor.Remove(item);
            _db.SaveChanges();
            return item;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<Investor> Get()
        {
            return _db.Investor.OrderBy(x => x.Id).ToList();
        }

        public Investor Get(int id)
        {
            return _db.Investor.Include(x => x.Portfolio).Where(x => x.Id == id).FirstOrDefault();
        }

        public Investor Update(Investor entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();
            return entity;
        }
    }
}

using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.Interfaces.Repositories;
using EnterprisesApi.Infrastructure.DataContexts;
using Microsoft.EntityFrameworkCore;

namespace EnterprisesApi.Infrastructure.Repositories
{
    public partial class PortfolioRepository : IPortfolioRepository
    {
        private readonly EnterprisesContext _db;

        public PortfolioRepository(EnterprisesContext enterprisesContext)
        {
            _db = enterprisesContext;
        }

        public Portfolio Create(Portfolio entity)
        {
             _db.Portfolio.Add(entity);
            _db.SaveChanges();
            return entity;
        }

        public Portfolio Delete(int id)
        {
            var item = _db.Portfolio.FirstOrDefault(a => a.Id == id);
            _db.Portfolio.Remove(item);
            _db.SaveChanges();
            return item;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<Portfolio> Get()
        {
            return _db.Portfolio.OrderBy(x => x.Id).ToList();
        }

        public Portfolio Get(int id)
        {
            return _db.Portfolio.Where(x => x.Id == id).FirstOrDefault();
        }

        public Portfolio Update(Portfolio entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();
            return entity;
        }
    }
}

using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using EnterprisesApi.Domain.Models;
using EnterprisesApi.Domain.Interfaces.Repositories;
using EnterprisesApi.Infrastructure.DataContexts;
using Microsoft.EntityFrameworkCore;

namespace EnterprisesApi.Infrastructure.Repositories
{
    public partial class UserRepository : IUserRepository
    {
        private readonly EnterprisesContext _db;

        public UserRepository(EnterprisesContext enterprisesContext)
        {
            _db = enterprisesContext;
        }

        public User Auth(string email, string password)
        {
            return _db.User.Where(x => x.Email.Equals(email) && x.Password.Equals(password)).FirstOrDefault();
        }

        public User ValidateAccess(string accessToken, string client, string uid)
        {
            return _db.User.Where(x => x.AccessToken.Equals(accessToken) && x.Client.Equals(client) && x.Email.Equals(uid)).FirstOrDefault();
        }

        public User Create(User entity)
        {
            _db.User.Add(entity);
            _db.SaveChanges();
            return entity;
        }

        public User Delete(int id)
        {
            var item = _db.User.FirstOrDefault(a => a.Id == id);
            _db.User.Remove(item);
            _db.SaveChanges();
            return item;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<User> Get()
        {
            return _db.User.OrderBy(x => x.Id).ToList();
        }

        public User Get(int id)
        {
            return _db.User.Where(x => x.Id == id).FirstOrDefault();
        }

        public User Update(User entity)
        {
             _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();
            return entity;
        }

        
    }
}

using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Domain.Interfaces.Repositories;
using EnterprisesApi.Infrastructure.DataContexts;
using Microsoft.EntityFrameworkCore;
using EnterprisesApi.Infrastructure.Repositories;

namespace EnterprisesApi.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private EnterprisesContext _db;

        private IEnterpriseRepository _enterpriseRepository;
        private IEnterpriseTypeRepository _enterpriseTypeRepository;
        private IInvestorRepository _investorRepository;
        private IPortfolioRepository _portfolioRepository;
        private IUserRepository _userRepository;
        
        public UnitOfWork()
        {
            _db = new EnterprisesContext();
            _db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public IEnterpriseRepository EnterpriseRepository
        {
            get
            {
                return _enterpriseRepository ?? (_enterpriseRepository = new EnterpriseRepository(_db));
            }
        }

        public IEnterpriseTypeRepository EnterpriseTypeRepository
        {
            get
            {
                return _enterpriseTypeRepository ?? (_enterpriseTypeRepository = new EnterpriseTypeRepository(_db));
            }
        }

        public IInvestorRepository InvestorRepository
        {
            get
            {
                return _investorRepository ?? (_investorRepository = new InvestorRepository(_db));
            }
        }

        public IPortfolioRepository PortfolioRepository
        {
            get
            {
                return _portfolioRepository ?? (_portfolioRepository = new PortfolioRepository(_db));
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                return _userRepository ?? (_userRepository = new UserRepository(_db));
            }
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Commit()
        {
            _db.SaveChanges();
            
        }
    }
}

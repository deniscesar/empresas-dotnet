﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EnterprisesApi.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnterpriseType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Enterprise_Type_Name = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterpriseType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Investor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Investor_Name = table.Column<string>(maxLength: 250, nullable: false),
                    Email = table.Column<string>(maxLength: 250, nullable: false),
                    City = table.Column<string>(maxLength: 250, nullable: false),
                    Country = table.Column<string>(maxLength: 250, nullable: false),
                    Balance = table.Column<decimal>(maxLength: 250, nullable: false),
                    Photo = table.Column<string>(maxLength: 250, nullable: true),
                    Portfolio_Value = table.Column<decimal>(nullable: false),
                    First_Access = table.Column<bool>(nullable: false),
                    Super_Angel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Investor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Portfolio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IdInvestor = table.Column<int>(nullable: false),
                    Enterprises_Number = table.Column<int>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portfolio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Portfolio_Investor_IdInvestor",
                        column: x => x.IdInvestor,
                        principalTable: "Investor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IdInvestor = table.Column<int>(nullable: false),
                    Email = table.Column<string>(maxLength: 250, nullable: false),
                    Password = table.Column<string>(maxLength: 250, nullable: false),
                    Client = table.Column<string>(maxLength: 250, nullable: true),
                    AccessToken = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Investor_IdInvestor",
                        column: x => x.IdInvestor,
                        principalTable: "Investor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enterprise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IdPortfolio = table.Column<int>(nullable: true),
                    IdEnterpriseType = table.Column<int>(nullable: false),
                    Email_Enterprise = table.Column<string>(maxLength: 250, nullable: true),
                    Facebook = table.Column<string>(maxLength: 250, nullable: true),
                    Twitter = table.Column<string>(maxLength: 250, nullable: true),
                    Linkedin = table.Column<string>(maxLength: 250, nullable: true),
                    Phone = table.Column<string>(maxLength: 250, nullable: true),
                    Own_Enterprise = table.Column<bool>(nullable: false),
                    Enterprise_Name = table.Column<string>(maxLength: 250, nullable: false),
                    Photo = table.Column<string>(maxLength: 250, nullable: false),
                    Description = table.Column<string>(maxLength: 250, nullable: false),
                    City = table.Column<string>(maxLength: 250, nullable: false),
                    Country = table.Column<string>(maxLength: 250, nullable: false),
                    Share_Price = table.Column<decimal>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enterprise_EnterpriseType_IdEnterpriseType",
                        column: x => x.IdEnterpriseType,
                        principalTable: "EnterpriseType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Enterprise_Portfolio_IdPortfolio",
                        column: x => x.IdPortfolio,
                        principalTable: "Portfolio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enterprise_IdEnterpriseType",
                table: "Enterprise",
                column: "IdEnterpriseType");

            migrationBuilder.CreateIndex(
                name: "IX_Enterprise_IdPortfolio",
                table: "Enterprise",
                column: "IdPortfolio");

            migrationBuilder.CreateIndex(
                name: "IX_Portfolio_IdInvestor",
                table: "Portfolio",
                column: "IdInvestor",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_IdInvestor",
                table: "User",
                column: "IdInvestor",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enterprise");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "EnterpriseType");

            migrationBuilder.DropTable(
                name: "Portfolio");

            migrationBuilder.DropTable(
                name: "Investor");
        }
    }
}

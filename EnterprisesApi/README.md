# README #

Informações sobre o projeto

### O foi utilizado no projeto ? ###

* .Net Core 2.0
* Entity Framework Core
* SGBD MySql
* Pomelo.EntityFrameworkCore.MySql
* UnitOfWork

### Deploy ###
* Necessário ter o .net cli 
* Execute dentro da pasta do projeto os seguintes comandos 
* ```dotnet restore```
* ```dotnet run```

### Informações ###
* Não foi utilizado token JWT devido aos header utilizados na API exemplo.
* O projeto encontra-se com um servidor de banco de dados para teste online.
* String de conexão encontra-se no diretorio /Infraestructure/DataContexts/EnterprisesContext.cs
* Em caso de configurar outro servidor de banco de dados vazio execute o migrations atraves do comando ```dotnet ef database update``` para migrar o esquema do banco de dados.
* Após migrado deve-se inserir os dados usando o arquivo dump.sql que esta na raiz do projeto.

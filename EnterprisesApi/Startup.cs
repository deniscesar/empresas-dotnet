﻿using EnterprisesApi.Authentication;
using EnterprisesApi.Business.Services;
using EnterprisesApi.Domain.Helpers;
using EnterprisesApi.Domain.Interfaces.Services;
using EnterprisesApi.Domain.Interfaces.UnitOfWork;
using EnterprisesApi.Infrastructure.DataContexts;
using EnterprisesApi.Infrastructure.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace EnterprisesApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CustomAuthOptions.DefaultScheme;
                options.DefaultChallengeScheme = CustomAuthOptions.DefaultScheme;
            })
            .AddCustomAuth(options =>
            {
                // Configure custom authentication
            });

            services.AddMvc()
                    .AddJsonOptions(opts =>
                    {
                        opts.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                        opts.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                        opts.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
                        opts.SerializerSettings.ContractResolver = new LowercaseContractResolver();
                    });

            ConfiguraDependencias(services);
        }

        private void ConfiguraDependencias(IServiceCollection services)
        {
            services.AddDbContext<EnterprisesContext>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            //Service
            services.AddTransient<IEnterpriseService, EnterpriseService>();
            services.AddTransient<IEnterpriseTypeService, EnterpriseTypeService>();
            services.AddTransient<IInvestorService, InvestorService>();
            services.AddTransient<IPortfolioService, PortfolioService>();
            services.AddTransient<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
